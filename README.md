Map Cleaner Manager
===================
Un outil de suivi du nettoyage de cartes pour le jeu [Aaaah !](http://www.extinction.fr/minijeux/)
  
![Demo](http://i.imgur.com/ACSxTID.png)

Authentification
================

La première utilisation de l'application nécessite l'authentification de l'utilisateur via une saisie.
Si la saisie correspond à un utilisateur prévu (i.e. présent dans la liste des volontaires), une session est créée
et l'utilisateur peut commencer à utiliser l'application. L'application se ferme si l'utilisateur ne s'authentifie pas.

Utilisation
===========
Nouvelle session
----------------
* __Importer X maps__ : Importe une playlist de 50, 100 ou 200 maps. Ces playlists sont stockées dans l'application
et l'outils d'import permet de charger le contenu de l'une d'entre elle dans une nouvelle session.
Il n'est possible d'importer que si aucune session est en cours (i.e. la session précédente à été archivée).

Actions
-------
* __Rechercher__ :
  Barre de recherche. Entrez n'importe quel mot et s'il apparait quelque part dans une map (code, auteur,
  titre, status, etc.), celui-ci apparaîtra. Accessible via __Ctrl + F__.

* __Playlist__ : 
  Permet l'enregistrement d'un fichier au format csv contenant les maps de la session en cours,
  à charger via /playlist (décocher aléatoire). La playlist reprend uniquement le code de la map.
  __Note__ : Cette fonction est obsolète, il est préférable d'utiliser les raccourcis clavier pour load
  rapidement une carte via __Ctrl + C__.

* __Archiver__ : 
  Vide la liste des maps pour la stocker à des fins de revues et de statistiques. Il n'est possible d'archiver 
  que si l'ensemble des maps traités (status ne contenant pas "todo" ou "À faire").

Raccourcis pratiques
====================
* __Ctrl + F__ :
  Permet d'avoir le focus directement sur la barre de recherche et sélectionne le texte déjà présent.

* __Double-clic sur une ligne__ : 
  Copie dans le presse-papiers le contenu de la cellule présente sous le curseur.
  Permet par exemple de copier l'auteur de la map.

* __Sélectionner une ligne -> Ctrl + C__ :
  Copie "/load @code" dans le presse-papiers où @code est le code correspondant à la map.

* __Sélectionner une ligne -> Entrée__ :
  Ouvre une nouvelle fenêtre permettant d'éditer le statut et d'ajouter un commentaire concernant la map. __Entrée__ pour valider,
  __Échap__ pour annuler.