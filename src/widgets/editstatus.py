# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
import json

from .modaldialog import ModalDialog


class EditStatusDialog(ModalDialog):
    def __init__(self, parent, original_status, original_comment, title=None):
        self.status = original_status
        self.comment = original_comment
        self.status_list = []
        with open("resources/tags.json", 'r', encoding='utf-8') as f:
            self.tags = json.load(f)
        for index in sorted(self.tags):
            for keyword in self.tags[index]:
                self.status_list.append(keyword)
        ModalDialog.__init__(self, parent, title)

    def body(self, master):
        content_frame = tk.Frame(master)
        content_frame.pack()
        self.mb = tk.Menubutton(content_frame, text="Statut", relief=tk.RAISED, width=15)
        self.mb.menu = tk.Menu(self.mb, tearoff=0)
        self.mb["menu"] = self.mb.menu
        self.mb.pack(side="left", anchor="n")
        self.Items= []
        i = 0
        for var in self.status_list:
            is_checked = 0
            if var in self.status :
                is_checked = 1
            self.Items.append( { "variable": tk.IntVar(),
                                 "name" : var })
            self.mb.menu.add_checkbutton(label=self.Items[i]["name"],
                                    variable=self.Items[i]["variable"],
                                    command=self.button_check_click)
            self.Items[i]["variable"].set(is_checked)
            i += 1

        self.mb.pack()
        right_frame = tk.Frame(content_frame)
        right_frame.pack(side="right")
        self.label = tk.Label(right_frame, text="Commentaire :")
        self.label.pack(side="top")
        self.text = tk.Text(right_frame, width=60, height=2, wrap="word", font=("Segoe UI", 9), exportselection=False,
                            undo=True, autoseparators=True, maxundo=-1)
        self.text.pack(side="top", pady=5, padx=5)
        self.text.bind('<Return>', lambda _: self.on_enter())
        self.text.bind('<Right>', lambda _: self.text.mark_set(tk.INSERT, tk.END))

        self.text.insert(tk.INSERT, self.comment)
        self.text.edit_reset() # reset undo/redo stack so that ctrl+z doesn't delete the original status
        self.select_all(None)
        return self.text


    def button_check_click(self):
        self.status = []
        for item in self.Items:
            if item["variable"].get() == 1:
                self.status.append(item["name"])

    def buttonbox(self):
        box = tk.Frame(self)
        box.pack()
        w = ttk.Button(box, text="OK", width=10, command=self.ok, default=tk.ACTIVE)
        w.pack(side=tk.LEFT, padx=5, pady=5)
        w = ttk.Button(box, text="Annuler", width=10, command=self.cancel)
        w.pack(side=tk.LEFT, padx=5, pady=5)


    def set_result(self):
        if len(self.status) == 0:
            self.status = [ "todo" ]
        self.result = {"status": self.status,
                       "commentary": self.text.get("1.0", tk.END).strip()}
    def on_enter(self):
        # Stores widget's content before a newline is inserted or the text removed
        self.set_result()

    def apply(self):
        if not self.result:
            # Clicked on 'Ok' instead of pressing Enter
            self.set_result()

    def select_all(self, event):
        self.text.tag_add(tk.SEL, "1.0", tk.END)
        self.text.mark_set(tk.INSERT, "1.0")
        self.text.see(tk.INSERT)
        return 'break'
