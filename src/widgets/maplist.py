# -*- coding: utf-8 -*-
# !python3

import json
import logging
# import winsound
import pyperclip

from widgets.treelist import Treelist
from widgets.popup import Popup
from widgets.editstatus import EditStatusDialog
from map import Map

logger = logging.getLogger(__name__)


class Maplist(Treelist):
    def __init__(self, master, maps, *args, **kwargs):
        Treelist.__init__(self, master, *args, **kwargs)
        self.maps = maps
        self.respomap = ""
        self._keys = {
            0: lambda x: 0,
            1: lambda x: x.datetime(),
            2: lambda x: x.code,
            3: lambda x: x.auteur.lower(),
            4: lambda x: x.titre.lower(),
            5: lambda x: x.statut.lower(),
            6: lambda x: x.commentaire.lower(),
            7: lambda x: x.respo.lower()
        }
        self._entry_edit = None
        self.last_popup = None
        self.tree.bind('<Double-1>', self.on_doubleclick)
        self.tree.bind('<Return>', self.on_enter)
        self.tree.bind('<Control-c>', self.copy)
        self.tree.bind('<FocusOut>', self.remove_popup)
        self.tree.bind('<<TreeviewSelect>>', self.remove_popup)
        self.update_tags()

    def update_tags(self):
        with open("resources/tags.json", 'r', encoding='utf-8') as f:
            self.tags = json.load(f)
        for index in self.tags:
            for keyword in self.tags[index]:
                if not self.tags[index][keyword] == "":
                    self.tree.tag_configure(keyword, background=self.tags[index][keyword])

    def insert(self, values, update=True, tags=None):
        tags = []
        try:
            for index in sorted(self.tags):
                for keyword in self.tags[index]:
                    if keyword in values[-3]:
                        tags.append(keyword)
                        raise StopIteration
        except StopIteration:
            pass
        super().insert(values, update, tags)

    def delete(self):
        if self.tree.selection():
            for item in self.tree.selection():
                values = self.tree.item(item)['values']
                values[0] = str(values[0])  # Treeviews force str to int if it's a digit
                values[-1] = [respo.strip() for respo in values[-1].split(",")] if values[-1] else []
                map = Map(*values[1:])
                self.maps.remove(map)
                logger.debug("Deleting {}".format(map))
            index = super().delete()
            self.refresh()
            if self._search_key.get() != '':
                self.search()
            self.focus_index(index)

    def selection_indexes(self):
        indexes = []
        selection = self.tree.selection()
        for item in selection:
            indexes.append(int(self.tree.item(item)['values'][0]) - 1)
        return indexes

    def sort(self, col, descending):
        if self.sortable:
            index = self.headers.index(col)
            if index == 0:
                self.maps.reverse()
            else:
                self.maps.sort(reverse=descending, key=self._keys[index])
            super().sort(col, descending)

    def search(self, key=None):
        key = key.strip() if key is not None else self._search_key.get().strip()
        if key == '':
            self.refresh()
        else:
            super().search(key)

    def on_doubleclick(self, event):
        if self.tree.identify_region(event.x, event.y) == "cell":
            # Clipboard
            item = self.tree.identify("item", event.x, event.y)
            column = int(self.tree.identify("column", event.x, event.y)[1:]) - 1
            value = str(self.tree.item(item)['values'][column])
            pyperclip.copy(value)
            # Popup
            x, y = self.master.winfo_pointerx(), self.master.winfo_pointery()
            msg = value if len(value) <= 20 else value[:20] + "..."
            Popup('"{}" copié dans le presse-papiers'.format(msg), x, y, delay=50, txt_color="white",
                  bg_color="#111111")

    def on_enter(self, event):
        select = self.tree.selection()
        if select:
            item = select[0]
            item_index = self.tree.get_children().index(item)
            values = self.tree.item(item)['values']
            values[0] = str(values[0])  # Treeviews force str to int if it's a digit
            data_index = self._data.index(values)
            # TODO Issue #7 retrieve statut and commentary from dialog
            origin_status = values[-3].split(", ")
            origin_commentary = values[-2]
            dialog = EditStatusDialog(self, origin_status, origin_commentary, "Éditer statut #{} : {}".format(values[0], values[2]))
            result = dialog.result
            if result != None:
                new_statut = result["status"]
                new_commentary = result["commentary"]
                if new_statut != origin_status or new_commentary != origin_commentary:
                    values[-1] = [respo.strip() for respo in values[-1].split(",")] if values[-1] else []
                    map = Map(*values[1:])
                    map_index = self.maps.index(map)
                    respo = self.respomap
                    if respo != '' and respo not in map.respo:
                        map.respo.append(respo)
                    map.statut = new_statut
                    map.commentaire = new_commentary
                    new_values = list(map.fields())
                    new_values.insert(0, values[0])
                    new_values[-1] = ", ".join(new_values[-1])
                    new_values[-3] = ", ".join(new_values[-3])
                    self._data[data_index] = new_values
                    self.maps[map_index] = map
                    self.refresh()
                    self.focus_index(item_index)
                else:
                    self.focus_item(item)

    def copy(self, event):
        selection = self.tree.selection()
        if len(selection) == 1:
            item = selection[0]
            load = "/load "
            load += self.tree.item(item)['values'][2]
            pyperclip.copy(load)
            try:
                x, y = self.tree.bbox(item, "code")[:2]
                x = x + self.winfo_rootx()
                y = y + self.winfo_rooty() - 21
                Popup('"{}" copié dans le presse-papiers'.format(load), x, y, delay=50, offset=(0, 0),
                      txt_color="white",
                      bg_color="#111111")
            except ValueError:
                pass

    def remove_popup(self, *args):
        if self.last_popup:
            self.last_popup.destroy()

    def populate(self):
        for i, map in enumerate(self.maps):
            f = list(map.fields())
            f[-3] = ", ".join(f[-3])
            f[-1] = ", ".join(f[-1])
            self.insert(f)

    def refresh(self):
        self.clear()
        self.populate()
        self.update_tags()
