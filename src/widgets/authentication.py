# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
from .modaldialog import ModalDialog


class AuthenticationDialog(ModalDialog):
    def __init__(self, parent, title=None, voluntaries=[]):
        self.voluntaries = voluntaries
        ModalDialog.__init__(self, parent, title, can_resize=False)
        self.initial_focus = self.text

    def body(self, master):
        self.text_frame = tk.Frame(master)

        self.text = ttk.Entry(self.text_frame, width=30, font=("Segoe UI", 9),
                             exportselection=False)
        self.text.pack(side=tk.LEFT)

        self.bind("<Return>", self.ok)
        self.text_frame.grid(row=0, column = 0, sticky="nswe")

        return self.text

    def buttonbox(self):
        self.button = ttk.Button(self.text_frame, text="OK", width=10, command=self.ok)
        self.button.pack(side=tk.RIGHT, padx=5, pady=5)

    def get_value(self):
        # get value with upper case only for the first letter of each words with title
        return self.text.get().title()

    def validate(self):
        self.result = self.get_value()
        if self.result in self.voluntaries:
            return True
        return False

    def clear_text(self):
        self.text.delete(0, 'end')

    def ok(self, event=None):
        if not self.validate():
            # Issue #1 error case
            self.clear_text()
            self.text.focus_set()
            return

        self.withdraw()
        self.update_idletasks()
        self.apply()
        self.close_dialog()

    def close_dialog(self):
        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    def cancel(self, event=None):
        # Overrided function
        # Close modal and parent window (allow user to exit the program)
        self.close_dialog()
        self.parent.quit()