# -*- coding: utf-8 -*-
# !python3

import re
import logging
import datetime
import os
import csv

from utils import log_args
from map import Map

logger = logging.getLogger(__name__)


def code_to_date(code):
    str_code = str(code)
    if str_code.startswith("@"):
        str_code = str_code[1:]
    int_code = int(str_code)
    return datetime.datetime.fromtimestamp(int_code/1000.0).strftime("%Y/%m/%d")


@log_args(logger=logger)
def parse(file):
    # TODO Issue #9 (De)Serializer tool
    # deserialize before parsing
    maps = []

    if not os.path.exists(file):
        return maps

    reader = csv.DictReader(open(file), delimiter = ";")
    for row in reader:
        map_code = row["code"]
        regex = "@?([0-9]{13})"
        if re.match(regex, map_code):
            author = row["auteur"]
            title = row["titre"]
            date = code_to_date(map_code)
            m = Map(date, map_code, author, title)
            if m in maps:
                maps.remove(m)
            maps.append(m)

    return maps

