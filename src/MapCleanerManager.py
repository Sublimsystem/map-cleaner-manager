# -*- coding: utf-8 -*-
# !python3

# from tkinter import *

import os
import json
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as mbox
import tkinter.filedialog as fdialog
import logging
from os.path import join

import utils
import map
import playlistparser
from widgets import maplist, modaldialog, customentries, statusbar
from widgets.authentication import AuthenticationDialog
from _version import __version__

__author__ = "Jinai"

class MapCleanerManager(tk.Tk):
    def __init__(self, master=None, warning=True, warning_msg=""):
        # Init var
        tk.Tk.__init__(self, master)
        self.master = master
        self.session_path = ""
        self.warning = warning
        self.warning_msg = warning_msg
        self.current_respo = ""
        self.current_playlist = ""
        self.is_session_in_progress = False
        self.is_session_completed = False
        with open("resources/respomaps.json", 'r', encoding='utf-8') as f:
            self.respomaps = json.load(f)
        self.maps = []
        # Rendering
        self._setup_widgets()
        self.title("Map Cleaner Manager v" + __version__)
        self.update_idletasks()
        self.minsize(600, self.winfo_reqheight())
        try:
            self.tk.call('encoding', 'system', 'utf-8')
            self.iconbitmap("resources/respotool.ico")
        except Exception as e:
            logging.error(e)

        # Issue #2 - Retrieve user and session from cookie
        user_name = utils.read_cookie()
        if user_name != "" and user_name in self.respomaps["voluntaries"]:
            self.setting_current_respo(user_name)
            self.init_session(self.current_respo)


        if self.current_respo == "" or self.session_path == "":
            dialog = AuthenticationDialog(self, "Saisir votre pseudo", self.respomaps["voluntaries"])
            if dialog.result in self.respomaps["voluntaries"]:
                self.setting_current_respo(dialog.result)
                utils.init_cookie(self.current_respo)
                self.init_session(self.current_respo)

        self.is_session_in_progress = self.current_respo != "" and len(self.maps) > 0

        # Bindings
        self.bind('<Control-f>', lambda _: self.search())
        self.bind('<Control-q>', lambda _: self.quit())
        self.main_frame.bind('<Button-1>', lambda _: self.clear_focus())
        self.protocol("WM_DELETE_WINDOW", self.quit)

        # Warnings
        if self.warning:
            modaldialog.InfoModal(self, "MapCleanerManager v" + __version__, self.warning_msg, "J'ai compris")

    def _setup_widgets(self):
        self.statusbar = statusbar.StatusBar(self)
        self.statusbar.pack(side="bottom", fill="x")
        self.main_frame = tk.Frame(self)
        self.main_frame.pack(fill='both', expand=True, pady=5, padx=5)

        # ------------------------------------------------ IMPORT --------------------------------------------------- #

        self.labelframe_session = ttk.Labelframe(self.main_frame, text="Nouvelle session")
        self.button_file50 = ttk.Button(self.labelframe_session, text="Importer 50 maps",
                                   state= "disabled", command= lambda:self.import_playlist_file(50))
        self. button_file50.pack(fill="both", expand=True, side="left", padx=(7, 0), pady=(0, 7))

        self.button_file100 = ttk.Button(self.labelframe_session, text="Importer 100 maps",
                                    state= "disabled", command= lambda:self.import_playlist_file(100))
        self.button_file100.pack(fill="both", expand=True, side="left", padx=7, pady=(0, 7))

        self.button_file200 = ttk.Button(self.labelframe_session, text="Importer 200 maps",
                                    state= "disabled", command= lambda:self.import_playlist_file(200))
        self.button_file200.pack(fill="both", expand=True, side="right", padx=(0, 7), pady=(0, 7))

        # ------------------------------------------------- SEARCH --------------------------------------------------- #

        search_icon = tk.PhotoImage(file="resources/search.gif")
        self.entry_search = customentries.PlaceholderEntry(self.main_frame, placeholder=" Rechercher", icon=search_icon,
                                                           width=30)

        # -------------------------------------------------- MAPS --------------------------------------------------- #

        headers = ['date', 'code', 'auteur', 'titre', 'statut', 'commentaire', 'respomap']
        column_widths = [30, 70, 100, 90, 120, 85, 130, 70]
        sort_keys = [
            lambda x: int(x[0]),
            lambda x: x[0].lower(),
            lambda x: x[0].lower(),
            lambda x: x[0].lower(),
            lambda x: x[0].lower(),
            lambda x: x[0].lower(),
            lambda x: x[0].lower(),
            lambda x: x[0].lower()
        ]
        stretch = [False, True, True, True, True, True, True, True]
        self.tree_map = maplist.Maplist(self.main_frame, self.maps, headers,
                                        column_widths, sort_keys=sort_keys, stretch=stretch, sortable=False,
                                        auto_increment=True, search_excludes=["Rechercher"])
        self.entry_search.entry.configure(textvariable=self.tree_map._search_key)

        # ------------------------------------------------ ACTIONS ------------------------------------------------- #

        self.frame_actions = tk.Frame(self.main_frame)

        self.frame_treatment = ttk.Labelframe(self.frame_actions, text="Traitement")
        self.frame_treatment.pack(side="left", padx=(0,7))

        self.frame_fin_session = ttk.Labelframe(self.frame_actions, text="Finaliser session")
        self.frame_fin_session.pack(side="right")

        self.button_playlist = ttk.Button(self.frame_treatment, text="Playlist",
                                          state= "disabled", command=self.export_playlist_file)
        self.button_playlist.pack(padx=7, pady=(0,7))

        self.button_archive = ttk.Button(self.frame_fin_session, text="Archiver",
                                         state= "disabled", command=self.archive_session)
        self.button_archive.pack(padx=7, pady=(0,7))

        # ------------------------------------------- WIDGETS ACTIVATION -------------------------------------------- #

        self.update_widget_state()

        # -------------------------------------------- WIDGETS PLACEMENT -------------------------------------------- #

        self.labelframe_session.grid(row=0, column=0, columnspan=2, sticky="nsw")
        self.entry_search.grid(row=0, column=1, sticky="e", padx=(0, 17), pady=(15, 5))
        self.tree_map.grid(row=2, column=0, columnspan=2, sticky="nsew", pady=(0, 10))
        self.frame_actions.grid(row=3, column=0, columnspan=2, sticky="ns")

        self.main_frame.grid_rowconfigure(2, weight=1)
        self.main_frame.grid_columnconfigure((0, 1), weight=1)
        # Changes the widget stack order so that pressing Tab after setting the Respomap brings the focus directly to
        # the table instead of giving the focus to the search bar. Not doing so would clear the selected items in the
        # table upon entering the search bar, which is unwanted. This is particularly useful when one forgets to set
        # the Respomap value and is prompted with it before being able to edit a status.
        self.entry_search.lower()
        # Needed to rewrite the placeholder because we hooked an empty StringVar that erased it
        self.entry_search.focus_out(None)

    def setting_current_respo(self, respo_name):
        self.current_respo = respo_name
        self.tree_map.respomap = respo_name
        logging.debug("Respo={}".format(self.current_respo))

    def update_widget_state(self):
        self.is_session_in_progress = self.current_respo != "" and len(self.maps) > 0
        if self.is_session_in_progress:
            self.is_session_completed = False
            self.button_file50.state(["disabled"])
            self.button_file100.state(["disabled"])
            self.button_file200.state(["disabled"])
            self.button_playlist.state(["!disabled"])
            # TODO Issue #3 - Determine if session is completed to allow use of archive button
            self.button_archive.state(["!disabled"])
        else:
            self.button_file50.state(["!disabled"])
            self.button_file100.state(["!disabled"])
            self.button_file200.state(["!disabled"])
            self.button_playlist.state(["disabled"])
            self.button_archive.state(["disabled"])

    def init_session(self, current_respo):
        potential_session = os.path.join(".", utils.SESSION_DIR, utils.SESSION_DIR_IN_PROGRESS, current_respo + ".sig")
        if not os.path.isfile(potential_session):
            # Issue #2 - Create empty session for current respo
            path = os.path.join(".", utils.SESSION_DIR, utils.SESSION_DIR_IN_PROGRESS)
            self.save_session(path, current_respo + ".sig")
        self.import_session(potential_session)

    def import_session(self, file_path):
        if os.path.isfile(file_path):
            logging.info("Importing '{}'".format(file_path))
            self.session_path = file_path
            with open(file_path, "r", encoding="utf-8") as f:
                dicts = json.load(f)
            # Issue #2 Import playlist name from session (to archive session in regards of the playlist name)
            for key in dicts:
                if key == "playlist":
                    self.current_playlist = dicts[key]
                if key == "maps":
                    for d in dicts[key]:
                        self.maps.append(map.Map.from_dict(d))
            self.refresh(auto_scroll=False)
            self.statusbar.set("{} maps importées depuis '{}' pour la playlist '{}'".format(len(self.maps), file_path,
                                                                                      self.current_playlist))

    def save_session(self, dir_path, file_name):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        file_path = os.path.join(dir_path, file_name)
        logging.info("Saving session in file '{}'".format(file_path))
        map_list = []
        for i, map in enumerate(self.maps):
            d = map.ordered_dict()
            d.update({"#": i + 1})
            d.move_to_end("#", last=False)
            map_list.append(d)
        session = {}
        session["maps"] = map_list
        session["playlist"] = self.current_playlist
        with open(file_path, mode="w", encoding="utf-8") as f:
            f.write(json.dumps(session, indent=4,ensure_ascii=False))
        self.statusbar.set("{} maps exportées dans '{}' pour la playlist '{}'".format(len(self.maps),
                                                                                      file_path,
                                                                                      self.current_playlist))

    def archive_session(self):
        session_completed = True
        first_todo_map = ""
        # Issue #8 Ensure that session is in progress (respomap defined, map > 0)
        if not self.is_session_in_progress:
            error_message = "Il n'y a pas de session en cours.\nVeuillez utiliser la fonction d'import de playlist."
            modaldialog.InfoModal(self, "Erreur", error_message, "OK")
            return
        # Issue #8 Ensure that each maps in the current session have been treated
        for map in self.maps:
            if "todo" in map.statut:
                session_completed = False
                first_todo_map = map.code
                break;
        if not session_completed:
            error_message = "La session en cours n'a pas été terminée.\nMap [{}]".format(first_todo_map)
            modaldialog.InfoModal(self, "Erreur", error_message, "OK")
            return
        msg = "Êtes-vous sûr de vouloir archiver ces maps ?\nElles seront retirées de la liste une fois fait !"
        if mbox.askokcancel("Archiver {} map".format(len(self.maps)), msg, icon="warning", parent=self):
            path = os.path.join(".", utils.SESSION_DIR, utils.SESSION_DIR_IN_PROGRESS)
            self.save_session(path, self.current_respo + ".sig")
            utils.archive(self.current_playlist, self.current_respo)
            archived = len(self.maps)
            self.maps.clear()
            self.refresh()
            self.statusbar.set("{} maps archivés".format(archived))

    def import_playlist_file(self, size):
        imported_file = self.retrieve_playlist_file(size)
        if imported_file is not None and os.path.exists(imported_file):
            self.maps = playlistparser.parse(imported_file)
            self.current_playlist = os.path.basename(imported_file)
            self.refresh()
            msg = "Playlist de {} maps importée".format(len(self.maps))
            mbox.showinfo("Playlist", msg, parent=self)

    def retrieve_playlist_file(self, size):
        # Issue #4 Localize playlist based on it's size, move file
        dir_source_path = os.path.join(".", utils.PLAYLIST_DIR, utils.PLAYLIST_DIR_TODO, str(size))
        if not os.path.exists(dir_source_path):
            os.makedirs(dir_source_path)
            mbox.showerror("Erreur", "Erreur lors du chargement de la playlist", parent=self)
            logging.error("Erreur lors de l'import d'une playlist : le répertoire {} n'existait pas"
                          .format(dir_source_path))
            return None

        first_file = None
        files = [f for f in os.listdir(dir_source_path) if f.endswith(".csv")]
        files.sort(key= lambda x : str(x))
        if len(files) > 0 :
            first_file = files[0]
        if first_file is None:
            mbox.showerror("Erreur", "Il n'y a plus de playlist de cette taille disponible.",
                           parent=self)
            logging.error("Plus aucune playlist dans {}."
                          .format(dir_source_path))
            return None

        source_file = os.path.join(dir_source_path, first_file)
        dir_dest_path = os.path.join(".", utils.PLAYLIST_DIR, utils.PLAYLIST_DIR_IN_PROGRESS)
        if not os.path.exists(dir_source_path):
            os.makedirs(dir_dest_path)
        dest_file = os.path.join(dir_dest_path, first_file)
        os.rename(source_file, dest_file)
        return dest_file

    def export_playlist_file(self):
        # Issue #6 allow "save as" for export playlist
        f = tk.filedialog.asksaveasfile(mode = 'w', defaultextension = ".txt",
                                        filetypes = [('csv files', '.csv'), ('text files', '.txt')],
                                        initialfile = "Playlist_" + str(self.current_playlist))
        if f is None:  # asksaveasfile return `None` if dialog closed with "cancel".
            return
        for map in self.maps:
            f.write(str(map.code + ";") + "\n")
        f.close()
        self.statusbar.set("Playlist créée dans '{}'".format(f.name))

    def search(self):
        self.entry_search.focus()
        self.entry_search.select_range(0, 'end')

    def refresh(self, auto_scroll=True):
        logging.debug("Refreshing {} maps".format(len(self.maps)))
        self.tree_map.maps = self.maps
        self.tree_map.refresh()
        self.tree_map.search()
        if auto_scroll:
            self.tree_map.scroll_down()
        self.update_widget_state()

    def clear_focus(self):
        for item in self.tree_map.tree.selection():
            self.tree_map.tree.selection_remove(item)
        self.main_frame.focus_force()

    def quit(self):
        # Issue #11 Save session state if user authenticated
        if self.current_respo != "":
            path = os.path.join(".", utils.SESSION_DIR, utils.SESSION_DIR_IN_PROGRESS)
            self.save_session(path, self.current_respo + ".sig")
        logging.info("Exiting MapCleanerManager\n")
        logging.shutdown()
        raise SystemExit


if __name__ == '__main__':
    log_level = utils.init_logging("MapCleanerManager", "mcm.log")
    logging.info("Starting MapCleanerManager v{} with log_level={}".format(__version__, log_level))

    msg = ""
    app = MapCleanerManager(warning=False, warning_msg=msg)
    app.mainloop()
