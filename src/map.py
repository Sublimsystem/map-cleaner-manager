# -*- coding: utf-8 -*-
# !python3

from collections import OrderedDict
from datetime import date


class Map():

    def __init__(self, date, code, auteur, titre, statut=[ "todo" ], commentaire="", respo=None):
        self.date = date
        self.code = code
        self.auteur = auteur
        self.titre = titre
        self.statut = statut
        self.commentaire = commentaire
        self.respo = [] if respo is None else respo

    def fields(self):
        return self.date, self.code, self.auteur, self.titre, self.statut, self.commentaire, self.respo

    def ordered_dict(self):
        return OrderedDict(zip(['date', 'code', 'auteur', 'titre', 'statut', 'commentaire', 'respo'], self.fields()))

    @staticmethod
    def from_dict(d):
        return Map(d['date'], d['code'], d['auteur'], d['titre'], d['statut'], d['commentaire'], d['respo'])

    def __str__(self):
        return str(self.fields())

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.code == other.code


    def __ne__(self, other):
        return not self == other
